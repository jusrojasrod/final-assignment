# Final Assignment week 5

In this Assignment, you will demonstrate the data visualization skills you learned by completing this course.   Dashboard is based on the concept of demonstrating US Domestic Airline Flights Performance and Yearly average flight delay statistics for a given year ( 2005 to 2020). 

To run properly install the followig packages:

* python3 -m pip install pandas dash
* pip3 install httpx==0.20 dash plotly 
